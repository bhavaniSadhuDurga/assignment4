package main;

public class ShortestDistApplication {
	
	// find the shortest distance between closest pair
	
	public static void main(String[] args) {
		ShortestDistance shortDist = new ShortestDistance();
		int[] nums = {1,3,10,99,6,15,7,21,5};
		shortDist.findShortDist(nums);
	}
}
