package main;

import java.util.LinkedHashSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BathRoom {
	private final Lock lock = new ReentrantLock();
	private LinkedHashSet<Person> persons;
	private static BathRoom instance = new BathRoom();
	private static final int maxMenCapacity = 3;
	private static final int maxWomenCapacity = 2;
	private int currentMenCapacity,currentWomenCapacity = 0;
	private Gender currentGender;
	CountDownLatch latch = new CountDownLatch(10);
	private Condition waitingPersons = lock.newCondition();
	
	public void useBathroom(Person person) throws InterruptedException{
		if (this.persons.add(person)) {
            System.out.println(person.getNameOfPerson() + " entered inside the bathroom");
            if(Gender.MAN .equals(person.getGender())) {
            	currentMenCapacity++;
            }else if(Gender.WOMAN .equals(person.getGender())) {
            	currentWomenCapacity++;
            }
        }                
	}
	
	public void exitBathRoom(Person person) throws InterruptedException{
		lock.lock();
		try {
			if(persons.remove(person)) {
				System.out.println(person.getNameOfPerson() + " left the bathroom");
			}
			if (persons.isEmpty()) {
                System.out.println("Bathroom is empty");
                waitingPersons.signal();
            }
		} finally {
			lock.unlock();
		}
		
	}
	
	public boolean isBathRoomFull() throws InterruptedException {
		if((currentMenCapacity == maxMenCapacity) || (currentWomenCapacity == maxWomenCapacity)) {
			return true;
		}
		return false;
	}
	
	public void enterBathRoom(Person person) throws InterruptedException {
		lock.lock();
		try {
			if (persons.isEmpty()) {
	            this.currentGender = person.getGender();
	        }
			if(canEnter(person)) {
				useBathroom(person);
			}else {
				waitingPersons.await();
			}
		} finally {
			lock.unlock();
		}
		if (isInTheBathroom(person)) {
            try {
            	Thread.sleep(10000);// how much duration person can inside bathroom
            	exitBathRoom(person);
            } catch (InterruptedException ex) {
                
            }
        }
	}
	
	public boolean canEnter(Person person) throws InterruptedException{
		return !isBathRoomFull() && !isInTheBathroom(person) && (currentGender.equals(person.getGender()) || currentGender.equals(Gender.NONE));
	}

	public BathRoom() {
        this.currentGender = Gender.NONE;
        this.persons = new LinkedHashSet<>();
    }
	
	public static BathRoom getInstance() {
        return instance;
    }
	
	public Gender getCurrentGender() {
		return currentGender;
	}

	public void setCurrentGender(Gender currentGender) {
		this.currentGender = currentGender;
	}

	public boolean isInTheBathroom(Person person) {
		return this.persons.contains(person);
	}
	
}
