package main;

import java.util.Arrays;

public class ShortestDistance {

	public void findShortDist(int[] nums) {
		int dist = Integer.MAX_VALUE;
		String closestPair = "";
		for(int count=0; count < nums.length; count++) {
			for(int innerCount = count+1; innerCount < nums.length; innerCount++) {
				int curDist = Math.abs(Math.subtractExact(nums[count],nums[innerCount]));
				if(curDist < dist ) { 
					dist = curDist; 
					closestPair = nums[count] +" , "+nums[innerCount];
				}
			}
		}
		System.out.println(closestPair+" are the closest pair having distance "+dist);
	}

}
