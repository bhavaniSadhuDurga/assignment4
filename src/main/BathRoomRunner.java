package main;

public class BathRoomRunner implements Runnable{
    private final BathRoom bathRoom;
    private final Person person;
      
    public BathRoomRunner(Person person, BathRoom bathRoom) {
    	this.person = person;
        this.bathRoom = bathRoom;
    }
    
    @Override
    public void run() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
        	try {
        		if(bathRoom.canEnter(person)) {
        			bathRoom.enterBathRoom(person);
        		}
			} catch (InterruptedException e) {
				System.out.println(e);
			}
    }
}
