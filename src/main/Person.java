package main;

public class Person {
	private final String nameOfPerson;
	private final Gender gender;
    
    public Person(String nameOfPerson, Gender gender) {
        this.nameOfPerson = nameOfPerson;
        this.gender = gender;
    }

	public String getNameOfPerson() {
		return nameOfPerson;
	}



	public Gender getGender() {
		return gender;
	}
	
	 @Override
	    public String toString() {
	        return "Person {" + "name = " + this.nameOfPerson + ", gender = " + this.gender + '}'+"\n";
	    }


}
