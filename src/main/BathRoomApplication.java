package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class BathRoomApplication {

	// Unisex Bathroom Problem for both men and women
	// 3 men allowed at a time & 2 women allowed at a time
	// when men inside woment not allowed vice versa
	public static void main(String[] args) throws InterruptedException {
		int numberOfPerson = 7;
	
		List<Person> persons = new ArrayList<>();
		persons = generateListOfPersons(numberOfPerson);
		
		System.out.println(persons);
		BathRoom bathroom = BathRoom.getInstance();
		for(Person person : persons) {
			BathRoomRunner batRoomRunner = new BathRoomRunner(person, bathroom);
			Thread bathRoomThread = new Thread(batRoomRunner);
			bathRoomThread.start();
			//bathRoomThread.join();
		}
		/*for(Person person : persons) {
			BathRoomRunner batRoomRunner = new BathRoomRunner(person, bathroom);
			Thread bathRoomThread = new Thread(batRoomRunner);
			try {
				bathRoomThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}*/
		
	}

	private static List<Person> generateListOfPersons(int size) {
		int personCount = 0;
		Random ran = new Random();
		List<Person> persons = new ArrayList<>();
		while (personCount < size) {
			int ranNumber = ran.nextInt(2);
			personCount++;
			switch (ranNumber) {
			case 0:
				persons.add(new Person("WOMAN" + personCount, Gender.WOMAN));
				break;
			case 1:
				persons.add(new Person("MAN" + personCount, Gender.MAN));
				break;

			}
		}
		return persons;
	}

}
